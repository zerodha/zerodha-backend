# Zerodha - backend

####Usage: 
 + To run the tests `npm run test`
 + For continuous development `npm run dev`
 + For watching tests `npm run test-watch` **(requires vc)**

####Docs: 
 + For swagger json hit [localhost:3000/.well-known/swagger.json]()
 + For swagger explorer hit [localhost:3000/api-docs]()

#####*(Hope you will enjoy the apis)*
