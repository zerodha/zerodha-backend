import request from 'supertest';

export default () => {
	const fixture = {};
	beforeAll(async () => {
		process.env.PORT = 3000;
		const serverObjs = await require('../../src/bin/www').serverPromise;
		Object.assign(fixture, serverObjs);
		fixture.hitApi = () => request('localhost:3000');
	});

	afterAll(async () => {
		await new Promise((resolve, reject) =>
			fixture.server.close((e) => e ? reject(e) : resolve()));
	});
	
	return fixture;
};
