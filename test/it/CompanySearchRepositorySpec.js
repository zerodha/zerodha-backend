import nock from "nock";
import CompanyInfoParser from '../../src/repositories/parsers/CompanyInfoParser';
import CompanySearchRepository from "../../src/repositories/CompanySearchRepository";

describe('CompanyRepository', () => {
	let companySearchRepository, nockBase;
	
	beforeAll(async () => {
		companySearchRepository = new CompanySearchRepository({companyInfoParser: new CompanyInfoParser()});
		nockBase = nock("https://www1.nseindia.com");
	});
	
	it('should fetch company list', async () => {
		const responseBody = `



{"rows1":[{"CompanyValues":"BHEL  Bharat Heavy Electricals Limited","CompanyNames":"<a style='color:red;'><a style='color:red;'>BHEL<\\/a><\\/a><br>Bharat Heavy Electricals Limited"}],"success":"true","results1":1}

`;
		const scope = nockBase.post("/corporates/common/getCompanyList.jsp", "query=BHEL")
			.reply(200, responseBody);
		
		const companyMetas = await companySearchRepository.findCompanyMetas("BHEL");
		
		expect(companyMetas).toEqual([{
			symbol: 'BHEL',
			companyName: 'Bharat Heavy Electricals Limited'
		}]);
	});
});
