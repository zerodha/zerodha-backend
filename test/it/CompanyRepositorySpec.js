import CompanyRepository from '../../src/repositories/CompanyRepository';
import CompanyInfoParser from '../../src/repositories/parsers/CompanyInfoParser';
import nock from "nock";

describe('CompanyRepository', () => {
	let companyRepository, nockBase;
	
	beforeAll(async () => {
		companyRepository = new CompanyRepository({companyInfoParser: new CompanyInfoParser()});
		nockBase = nock("https://www1.nseindia.com");
	});
	
	it('should fetch company info', async () => {
		const responseBody = `
<TABLE cellspacing=0 border=0 cellpadding=0 bgcolor=#ffffff width=100% >
	<tr>
		<td class='t00_left'><center><b><nobr>Infosys Limited</nobr></b></center></td>
	</tr>
	<tr>
		<td><b>Date of Listing (NSE) : </b>08-Feb-1995</td>
	</tr>
	<tr>
		<td><b>Face Value : </b>        5.00</td>
	</tr>
	<tr>
		<td><b>ISIN : </b>INE009A01021</td>
	</tr>
	<tr>
		<td><b>Industry : </b>COMPUTERS - SOFTWARE</td>
	</tr>
	<tr>
		<td><b>Constituent Indices : </b>,NIFTY 500,NIFTY IT,NIFTY 50,NIFTY 100,NIFTY 200,NIFTY SERVICES SECTOR</td>
	</tr>
	<tr height=10>
		<td></td>
	</tr>
	<tr>
		<td><b>Issued Cap. : </b>      4258456678(shares) as on 03-Dec-2019</td>
	</tr>
	<tr>
		<td><b>Free Float Market Cap. *: </b> <img src = "/images/rup_t1.gif"  alt="Rs."/> 255752.14(Cr) </td>
	</tr>
	<tr>
		<td><b>Impact Cost: </b>   0.01 as on Nov-2019</td>
	</tr>
	<tr>
		<td><b>52 week high/low price : </b>847.00/615.10</td>
	</tr>
	<tr>
		<td><b>* </b> Free-float market capitalization as on the previous trading day. </td>
	</tr>
</table>
`;
		const scope = nockBase.get("/marketinfo/companyTracker/compInfo.jsp")
			.query({symbol: "ABCD XYZ", series: "EQ"})
			.reply(200, responseBody);
		
		const companyInfo = await companyRepository.getCompanyInfo({symbol: "ABCD XYZ"});
		
		expect(companyInfo).toEqual({
			title: 'Infosys Limited',
			info:
				[{key: 'Date of Listing (NSE)', value: '08-Feb-1995'},
					{key: 'Face Value', value: '5.00'},
					{key: 'ISIN', value: 'INE009A01021'},
					{key: 'Industry', value: 'COMPUTERS - SOFTWARE'},
					{
						key: 'Constituent Indices',
						value: ',NIFTY 500,NIFTY IT,NIFTY 50,NIFTY 100,NIFTY 200,NIFTY SERVICES SECTOR'
					},
					{key: 'Issued Cap.', value: '4258456678(shares) as on 03-Dec-2019'},
					{key: 'Free Float Market Cap. *', value: '255752.14(Cr)'},
					{key: 'Impact Cost', value: '0.01 as on Nov-2019'},
					{key: '52 week high/low price', value: '847.00/615.10'}],
			disclaimer:
				'*  Free-float market capitalization as on the previous trading day.'
		})
	});
	
	it('should fetch corporate actions', async () => {
		const responseBody = `
<TABLE cellspacing=0 border=0 cellpadding=0 bgcolor=#ffffff width=100% >
	<tr>
		<td class='t00CA_left'>&nbsp;</td>
		<td class='t00CA_left'>Ex-Date</td>
		<td class='t00CA_left'>&nbsp;&nbsp;</td>
		<td class='t00CA_left'>Purpose</td>
		<td class='t00CA_left'>&nbsp;</td>
	</tr>
	<tr valign=top>
		<td>&nbsp;</td>
		<td nowrap><b>23-Oct-2019</b></td>
		<td>&nbsp;:&nbsp;</td>
		<td> INTERIM DIVIDEND - RS 8 PER SHARE</td>
		<td>&nbsp;</td>
	</tr>
	<tr valign=top>
		<td>&nbsp;</td>
		<td nowrap><b>13-Jun-2019</b></td>
		<td>&nbsp;:&nbsp;</td>
		<td> ANNUAL GENERAL MEETING/DIVIDEND- RS 10.50 PER SHARE</td>
		<td>&nbsp;</td>
	</tr>
	<tr valign=top>
		<td>&nbsp;</td>
		<td nowrap><b>24-Jan-2019</b></td>
		<td>&nbsp;:&nbsp;</td>
		<td> SPECIAL DIVIDEND - RS 4 PER SHARE</td>
		<td>&nbsp;</td>
	</tr>
	<tr valign=top>
		<td>&nbsp;</td>
		<td nowrap><b>25-Oct-2018</b></td>
		<td>&nbsp;:&nbsp;</td>
		<td> INTERIM DIVIDEND - RS 7 PER SHARE</td>
		<td>&nbsp;</td>
	</tr>
	<tr valign=top>
		<td>&nbsp;</td>
		<td nowrap><b>04-Sep-2018</b></td>
		<td>&nbsp;:&nbsp;</td>
		<td> BONUS 1:1</td>
		<td>&nbsp;</td>
	</tr>
</table>
`;
		const scope = nockBase.get("/marketinfo/companyTracker/corpAction.jsp")
			.query({symbol: "ABCD XYZ"})
			.reply(200, responseBody);
		
		const corporateActions = await companyRepository.getCorporateActions({symbol: "ABCD XYZ"});
		
		expect(corporateActions).toEqual({
			header: ['Ex-Date', 'Purpose'],
			actions: [
				{
					exDate: '23-Oct-2019',
					purpose: 'INTERIM DIVIDEND - RS 8 PER SHARE'
				},
				{
					exDate: '13-Jun-2019',
					purpose: 'ANNUAL GENERAL MEETING/DIVIDEND- RS 10.50 PER SHARE'
				},
				{
					exDate: '24-Jan-2019',
					purpose: 'SPECIAL DIVIDEND - RS 4 PER SHARE'
				},
				{
					exDate: '25-Oct-2018',
					purpose: 'INTERIM DIVIDEND - RS 7 PER SHARE'
				},
				{
					exDate: '04-Sep-2018', purpose: 'BONUS 1:1'
				}
			]
		})
	});
	
	
	it('should fetch corporate announcements', async () => {
		const responseBody = `
 
<!--jsp:useBean id="conBean1" class="ConPool.ConnectionBean" /-->

<TABLE cellspacing=0 border=0 cellpadding=0 bgcolor=#ffffff>
	<tr valign=top>
		<td>&nbsp;</td>
		<td> - Analysts/Institutional Investor Meet/Con. Call Updates&nbsp;&nbsp;<a href="#" class=smalllinks onClick="javascript: window.open('/marketinfo/companyTracker/announceDetails.jsp?symbol=INFY&desc=Analysts%2FInstitutional+Investor+Meet%2FCon.+Call+Updates&tstamp=031220191340','test','left=0,top=0,fullscreen=yes,scrollbars=yes,titlebar=no,menubar=no')"><b>Dec 03, 2019, 13:40</b></a></td>
		<td>&nbsp;</td>
	</tr>
	<tr valign=top>
		<td>&nbsp;</td>
		<td> - Press Release&nbsp;&nbsp;<a href="#" class=smalllinks onClick="javascript: window.open('/marketinfo/companyTracker/announceDetails.jsp?symbol=INFY&desc=Press+Release&tstamp=291120191649','test','left=0,top=0,fullscreen=yes,scrollbars=yes,titlebar=no,menubar=no')"><b>Nov 29, 2019, 16:49</b></a></td>
		<td>&nbsp;</td>
	</tr>
	<tr valign=top>
		<td>&nbsp;</td>
		<td> - Press Release&nbsp;&nbsp;<a href="#" class=smalllinks onClick="javascript: window.open('/marketinfo/companyTracker/announceDetails.jsp?symbol=INFY&desc=Press+Release&tstamp=261120191516','test','left=0,top=0,fullscreen=yes,scrollbars=yes,titlebar=no,menubar=no')"><b>Nov 26, 2019, 15:16</b></a></td>
		<td>&nbsp;</td>
	</tr>
	<tr valign=top>
		<td>&nbsp;</td>
		<td> - Press Release&nbsp;&nbsp;<a href="#" class=smalllinks onClick="javascript: window.open('/marketinfo/companyTracker/announceDetails.jsp?symbol=INFY&desc=Press+Release&tstamp=211120191500','test','left=0,top=0,fullscreen=yes,scrollbars=yes,titlebar=no,menubar=no')"><b>Nov 21, 2019, 15:00</b></a></td>
		<td>&nbsp;</td>
	</tr>
	<tr valign=top>
		<td>&nbsp;</td>
		<td> - Press Release&nbsp;&nbsp;<a href="#" class=smalllinks onClick="javascript: window.open('/marketinfo/companyTracker/announceDetails.jsp?symbol=INFY&desc=Press+Release&tstamp=211120191415','test','left=0,top=0,fullscreen=yes,scrollbars=yes,titlebar=no,menubar=no')"><b>Nov 21, 2019, 14:15</b></a></td>
		<td>&nbsp;</td>
	</tr>
</table>

`;
		
		const scope = nockBase.get("/marketinfo/companyTracker/corpAnnounce.jsp")
			.query({symbol: "ABCD XYZ"})
			.reply(200, responseBody);
		
		const corpAnnouncements = await companyRepository.getCorporateAnnouncements({symbol: "ABCD XYZ"});
		
		expect(corpAnnouncements).toEqual([
			{
				url:
					'https://www1.nseindia.com/marketinfo/companyTracker/announceDetails.jsp?symbol=INFY&desc=Analysts%2FInstitutional+Investor+Meet%2FCon.+Call+Updates&tstamp=031220191340',
				details:
					'- Analysts/Institutional Investor Meet/Con. Call Updates  Dec 03, 2019, 13:40'
			},
			{
				url:
					'https://www1.nseindia.com/marketinfo/companyTracker/announceDetails.jsp?symbol=INFY&desc=Press+Release&tstamp=291120191649',
				details: '- Press Release  Nov 29, 2019, 16:49'
			},
			{
				url:
					'https://www1.nseindia.com/marketinfo/companyTracker/announceDetails.jsp?symbol=INFY&desc=Press+Release&tstamp=261120191516',
				details: '- Press Release  Nov 26, 2019, 15:16'
			},
			{
				url:
					'https://www1.nseindia.com/marketinfo/companyTracker/announceDetails.jsp?symbol=INFY&desc=Press+Release&tstamp=211120191500',
				details: '- Press Release  Nov 21, 2019, 15:00'
			},
			{
				url:
					'https://www1.nseindia.com/marketinfo/companyTracker/announceDetails.jsp?symbol=INFY&desc=Press+Release&tstamp=211120191415',
				details: '- Press Release  Nov 21, 2019, 14:15'
			}
		])
	});
});
