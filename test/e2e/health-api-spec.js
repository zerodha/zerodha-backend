import request from 'supertest';
import e2eTestSetup from '../fixtures/e2e-test-setup';

describe('Health Api', () => {
	const fixture = e2eTestSetup();
	
	it('should return with 200', async () => {
		
		await request(fixture.app)
			.get('/health')
			.expect('Content-Type', /json/)
			.expect(200, {status: 'UP'});
	});
});
