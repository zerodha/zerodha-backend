import nock from "nock";
import e2eTestSetup from '../fixtures/e2e-test-setup';

describe('Equity Api', () => {
	const fixture = e2eTestSetup();
	const nockBase = nock("https://www1.nseindia.com");
	
	it('should search equities within a given listing period', async () => {
		const nseApiResponseBody = `

//This is human friendly json, can not be parsed with JSON.parse()
{
	success: true,
	results: 362,
	rows: [
		{sym: "CSBBANK", isin: "INE679A01013", cmp: "CSB Bank Limited", ListDt: "04-Dec-2019", face: "10", paid: "10", mlot: "1", ind: "-"},
		{sym: "CHEMBOND", isin: "INE995D01025", cmp: "Chembond Chemicals Ltd", ListDt: "20-Nov-2019", face: "5", paid: "5", mlot: "1", ind: "-"},
	]
}

`;
		const scope = nockBase.post("/corporates/listDir/getListDirectEQ.jsp",
			"symbol=&listingPeriod=Last%205%20Years&start=0&limit=25&segment=EQUITIES")
			.reply(200, nseApiResponseBody);
		
		await fixture.hitApi()
			.get('/api/equities?symbol=&listing-period=L5Y&Industry=&segment=EQUITIES')
			.expect('Content-Type', /json/)
			.expect('Link', '</api/equities?symbol=&listing-period=L5Y&Industry=&segment=EQUITIES&start=25&limit=25>; rel="next",' +
				' </api/equities?symbol=&listing-period=L5Y&Industry=&segment=EQUITIES&start=337&limit=25>; rel="last"')
			.expect(200, [
				{
					listingDate: '04-Dec-2019',
					companyName: 'CSB Bank Limited',
					faceValue: '10',
					industry: '-',
					isin: 'INE679A01013',
					marketLot: '1',
					paidUpValue: '10',
					symbol: 'CSBBANK'
				},
				{
					listingDate: '20-Nov-2019',
					companyName: 'Chembond Chemicals Ltd',
					faceValue: '5',
					industry: '-',
					isin: 'INE995D01025',
					marketLot: '1',
					paidUpValue: '5',
					symbol: 'CHEMBOND'
				},
			]);
	});
});
