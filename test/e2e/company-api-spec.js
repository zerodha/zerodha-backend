import nock from "nock";
import e2eTestSetup from '../fixtures/e2e-test-setup';

describe('Company Api', () => {
	const fixture = e2eTestSetup();
	const nockBase = nock("https://www1.nseindia.com");
	
	
	it('should search using with text query', async () => {
		const nseApiResponseBody = `
{
	"rows1": [{
		"CompanyValues": "TCS  Tata Consultancy Services Limited",
		"CompanyNames": "<a style='color:red;'><a style='color:red;'>TCS<\\/a><\\/a><br>Tata Consultancy Services Limited"
	}, {
		"CompanyValues": "WSTCSTPAPR  West Coast Paper Mills Limited",
		"CompanyNames": "WS<a style='color:red;'><a style='color:red;'>TCS<\\/a><\\/a>TPAPR<br>West Coast Paper Mills Limited"
	}], "success": "true", "results1": 6
}
`;
		
		const scope = nockBase.post("/corporates/common/getCompanyList.jsp", "query=TCS")
			.reply(200, nseApiResponseBody);
		
		await fixture.hitApi()
			.get('/api/companies?query=TCS')
			.expect('Content-Type', /json/)
			.expect(200, [{
				symbol: 'TCS',
				companyName: 'Tata Consultancy Services Limited'
			}, {
				symbol: 'WSTCSTPAPR',
				companyName: 'West Coast Paper Mills Limited'
			}]);
	});
	
	const setupCompanyInfo = () => {
		const nseApiResponseBody = `
<TABLE cellspacing=0 border=0 cellpadding=0 bgcolor=#ffffff width=100% >
	<tr><td class='t00_left'><center><b><nobr>Tata Consultancy Services Limited</nobr></b></center></td></tr>
	<tr><td><b>Date of Listing (NSE) : </b>25-Aug-2004</td></tr>
	<tr><td><b>Face Value : </b>        1.00</td></tr>
	<tr><td><b>ISIN : </b>INE467B01029</td></tr>
	<tr><td><b>Industry : </b>COMPUTERS - SOFTWARE</td></tr>
	<tr><td><b>Constituent Indices : </b>,NIFTY 50,NIFTY 100,NIFTY SERVICES SECTOR,NIFTY 200,NIFTY 500,NIFTY IT</td></tr>
	<tr height=10><td></td></tr>
	<tr><td><b>Issued Cap. : </b>      3752384706(shares) as on 06-Dec-2019</td></tr>
	<tr><td><b>Free Float Market Cap. *: </b> <img src = "/images/rup_t1.gif"  alt="Rs."/> 223119.8(Cr) </td></tr>
	<tr><td><b>Impact Cost: </b>   0.02 as on Nov-2019</td></tr>
	<tr><td><b>52 week high/low price : </b>2296.20/1808.00</td></tr>
	<tr><td><b>* </b> Free-float market capitalization as on the previous trading day. </td></tr>
</table>

`;
		
		const scope = nockBase.get("/marketinfo/companyTracker/compInfo.jsp")
			.query({symbol: "TCS", series: "EQ"})
			.reply(200, nseApiResponseBody);
	};
	
	const setupCorporateActions = () => {
		const nseApiResponseBody = `
<TABLE cellspacing=0 border=0 cellpadding=0 bgcolor=#ffffff width=100% >
	<tr><td class='t00CA_left'>&nbsp;</td><td class='t00CA_left'>Ex-Date</td><td class='t00CA_left'>&nbsp;&nbsp;</td><td class='t00CA_left'>Purpose</td><td class='t00CA_left'>&nbsp;</td></tr>
	<tr valign=top><td>&nbsp;</td><td nowrap><b>04-Jun-2019</b></td><td>&nbsp;:&nbsp;</td><td> DIVIDEND - RS 18 PER SHARE</td><td>&nbsp;</td></tr>
	<tr valign=top><td>&nbsp;</td><td nowrap><b>17-Oct-2019</b></td><td>&nbsp;:&nbsp;</td><td> INTERIM DIVIDEND - RS 5 PER SHARE AND SPECIAL DIVIDEND - RS 40 PER SHARE</td><td>&nbsp;</td></tr>
	<tr valign=top><td>&nbsp;</td><td nowrap><b>17-Jan-2019</b></td><td>&nbsp;:&nbsp;</td><td> INTERIM DIVIDEND - RS 4 PER SHARE</td><td>&nbsp;</td></tr>
	<tr valign=top><td>&nbsp;</td><td nowrap><b>23-Oct-2018</b></td><td>&nbsp;:&nbsp;</td><td> INT DIV RS 4 PER SH</td><td>&nbsp;</td></tr>
	<tr valign=top><td>&nbsp;</td><td nowrap><b>16-Jul-2019</b></td><td>&nbsp;:&nbsp;</td><td> INTERIM DIVIDEND - RS 5 PER SHARE</td><td>&nbsp;</td></tr>
</table>

`;
		const scope = nockBase.get("/marketinfo/companyTracker/corpAction.jsp")
			.query({symbol: "TCS"})
			.reply(200, nseApiResponseBody);
	};
	
	const setupCorporateAnnouncements = () => {
		const nseApiResponseBody = `
<TABLE cellspacing=0 border=0 cellpadding=0 bgcolor=#ffffff>
	<tr valign=top><td>&nbsp;</td><td> - Press Release&nbsp;&nbsp;<a href="#" class=smalllinks onClick="javascript: window.open('/marketinfo/companyTracker/announceDetails.jsp?symbol=TCS&desc=Press+Release&tstamp=061220191614','test','left=0,top=0,fullscreen=yes,scrollbars=yes,titlebar=no,menubar=no')"><b>Dec 06, 2019, 16:14</b></a></td><td>&nbsp;</td></tr>
	<tr valign=top><td>&nbsp;</td><td> - Press Release&nbsp;&nbsp;<a href="#" class=smalllinks onClick="javascript: window.open('/marketinfo/companyTracker/announceDetails.jsp?symbol=TCS&desc=Press+Release&tstamp=041220191206','test','left=0,top=0,fullscreen=yes,scrollbars=yes,titlebar=no,menubar=no')"><b>Dec 04, 2019, 12:06</b></a></td><td>&nbsp;</td></tr>
	<tr valign=top><td>&nbsp;</td><td> - Press Release&nbsp;&nbsp;<a href="#" class=smalllinks onClick="javascript: window.open('/marketinfo/companyTracker/announceDetails.jsp?symbol=TCS&desc=Press+Release&tstamp=031220191453','test','left=0,top=0,fullscreen=yes,scrollbars=yes,titlebar=no,menubar=no')"><b>Dec 03, 2019, 14:53</b></a></td><td>&nbsp;</td></tr>
	<tr valign=top><td>&nbsp;</td><td> - Press Release&nbsp;&nbsp;<a href="#" class=smalllinks onClick="javascript: window.open('/marketinfo/companyTracker/announceDetails.jsp?symbol=TCS&desc=Press+Release&tstamp=031220191229','test','left=0,top=0,fullscreen=yes,scrollbars=yes,titlebar=no,menubar=no')"><b>Dec 03, 2019, 12:29</b></a></td><td>&nbsp;</td></tr>
	<tr valign=top><td>&nbsp;</td><td> - Press Release&nbsp;&nbsp;<a href="#" class=smalllinks onClick="javascript: window.open('/marketinfo/companyTracker/announceDetails.jsp?symbol=TCS&desc=Press+Release&tstamp=021220191617','test','left=0,top=0,fullscreen=yes,scrollbars=yes,titlebar=no,menubar=no')"><b>Dec 02, 2019, 16:17</b></a></td><td>&nbsp;</td></tr>
</table>
`;
		
		const scope = nockBase.get("/marketinfo/companyTracker/corpAnnounce.jsp")
			.query({symbol: "TCS"})
			.reply(200, nseApiResponseBody);
	};
	
	
	it('should get company details using ticker symbol', async () => {
		setupCompanyInfo();
		setupCorporateActions();
		setupCorporateAnnouncements();
		
		const expectedInsightsResponse = {
			companyInfo: {
				title: "Tata Consultancy Services Limited",
				info: [
					{
						key: "Date of Listing (NSE)",
						value: "25-Aug-2004"
					},
					{
						key: "Face Value",
						value: "1.00"
					},
					{
						key: "ISIN",
						value: "INE467B01029"
					},
					{
						key: "Industry",
						value: "COMPUTERS - SOFTWARE"
					},
					{
						key: "Constituent Indices",
						value: ",NIFTY 50,NIFTY 100,NIFTY SERVICES SECTOR,NIFTY 200,NIFTY 500,NIFTY IT"
					},
					{
						key: "Issued Cap.",
						value: "3752384706(shares) as on 06-Dec-2019"
					},
					{
						key: "Free Float Market Cap. *",
						value: "223119.8(Cr)"
					},
					{
						key: "Impact Cost",
						value: "0.02 as on Nov-2019"
					},
					{
						key: "52 week high/low price",
						value: "2296.20/1808.00"
					}
				],
				disclaimer: "*  Free-float market capitalization as on the previous trading day."
			},
			corporateActions: {
				header: [
					"Ex-Date",
					"Purpose"
				],
				actions: [
					{
						exDate: "2019-10-16T18:30:00.000Z",
						purpose: "INTERIM DIVIDEND - RS 5 PER SHARE AND SPECIAL DIVIDEND - RS 40 PER SHARE"
					},
					{
						exDate: "2019-07-15T18:30:00.000Z",
						purpose: "INTERIM DIVIDEND - RS 5 PER SHARE"
					},
					{
						exDate: "2019-06-03T18:30:00.000Z",
						purpose: "DIVIDEND - RS 18 PER SHARE"
					},
					{
						exDate: "2019-01-16T18:30:00.000Z",
						purpose: "INTERIM DIVIDEND - RS 4 PER SHARE"
					},
					{
						exDate: "2018-10-22T18:30:00.000Z",
						purpose: "INT DIV RS 4 PER SH"
					}
				]
			},
			corporateAnnouncements: [
				{
					url: "https://www1.nseindia.com/marketinfo/companyTracker/announceDetails.jsp?symbol=TCS&desc=Press+Release&tstamp=061220191614",
					details: "- Press Release  Dec 06, 2019, 16:14"
				},
				{
					url: "https://www1.nseindia.com/marketinfo/companyTracker/announceDetails.jsp?symbol=TCS&desc=Press+Release&tstamp=041220191206",
					details: "- Press Release  Dec 04, 2019, 12:06"
				},
				{
					url: "https://www1.nseindia.com/marketinfo/companyTracker/announceDetails.jsp?symbol=TCS&desc=Press+Release&tstamp=031220191453",
					details: "- Press Release  Dec 03, 2019, 14:53"
				},
				{
					url: "https://www1.nseindia.com/marketinfo/companyTracker/announceDetails.jsp?symbol=TCS&desc=Press+Release&tstamp=031220191229",
					details: "- Press Release  Dec 03, 2019, 12:29"
				},
				{
					url: "https://www1.nseindia.com/marketinfo/companyTracker/announceDetails.jsp?symbol=TCS&desc=Press+Release&tstamp=021220191617",
					details: "- Press Release  Dec 02, 2019, 16:17"
				}
			]
		};
		
		await fixture.hitApi()
			.get('/api/companies/TCS/insights')
			.expect('Content-Type', /json/)
			.expect(200, expectedInsightsResponse);
	});
});
