import express from 'express';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import {createContainer, Lifetime} from 'awilix';
import {loadControllers, scopePerRequest} from 'awilix-express';
import errorHandler from './errors/error-handler';
import swaggerRouter from './services/swagger';


export const initApp = async () => {
	const app = express();
	
	app.use(logger('dev'));
	app.use(express.json());
	app.use(express.urlencoded({ extended: false }));
	app.use(cookieParser());
	
	
	const container = createContainer();
	container.loadModules([
		['services/**/*.js', {
			lifetime: Lifetime.SINGLETON
		}],
		['repositories/**/*.js', {
			lifetime: Lifetime.SINGLETON
		}],
	], {formatName: 'camelCase', cwd: __dirname});

	app.use(scopePerRequest(container));
	// Loads all controllers in the `routes` folder
	// relative to the current working directory.
	// This is a glob pattern.
	app.use(loadControllers('routes/*.js', { cwd: __dirname }));
	
	//Api documentations
	app.use(swaggerRouter);
	//Must be the last handler
	app.use(errorHandler);

	return {app, container};
};

