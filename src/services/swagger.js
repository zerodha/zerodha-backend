import swaggerUi from 'swagger-ui-express';
import swaggerJSDoc from 'swagger-jsdoc';
import {Router} from 'express';

const options = {
	definition: {
		info: {
			title: 'Zerodha Backend', // Title (required)
			version: '1.0.0', // Version (required)
		},
		
	},
	// Path to the API docs
	apis: [`${__dirname}/../routes/**.*`],
};

// Initialize swagger-jsdoc -> returns validated swagger spec in json format
const swaggerSpec = swaggerJSDoc(options);

const swaggerRouter = Router();
swaggerRouter.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec, {explorer: true}));
swaggerRouter.use('/.well-known/swagger.json', (req, res) => res.json(swaggerSpec));

export default swaggerRouter;
