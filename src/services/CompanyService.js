import Company from "../domain/models/Company";

const DEFAULT_RESULT_SET_LIMIT = 25;

const resourceBoundsIfPossible = (start, totalResultsCount) => {
	start = start >= 0 ? start : 0;
	const limit = Math.min(DEFAULT_RESULT_SET_LIMIT, totalResultsCount - start);
	return (start < totalResultsCount) && {start, limit};
};

export default class CompanyService {
	_companyRepository;
	_companySearchRepository;
	
	constructor({companyRepository, companySearchRepository}) {
		this._companyRepository = companyRepository;
		this._companySearchRepository = companySearchRepository;
	}
	
	async getCompanyBySymbol(symbol) {
		const [companyInfo,
			corporateActions,
			corporateAnnouncements,
		] = await Promise.all([
			this._companyRepository.getCompanyInfo({symbol}),
			this._companyRepository.getCorporateActions({symbol}),
			this._companyRepository.getCorporateAnnouncements({symbol}),
		]);
		
		return new Company(symbol, {
			companyInfo,
			corporateActions,
			corporateAnnouncements,
		});
	}
	
	async getCompanyInsightsBySymbol(symbol) {
		const company = await this.getCompanyBySymbol(symbol);
		return company.getInsights();
	}
	
	async searchEquities({symbol = "", listingPeriod = "", start = 0, limit = DEFAULT_RESULT_SET_LIMIT}) {
		const {
			totalResultsCount, currentResults
		} = await this._companySearchRepository.findEquities({symbol, listingPeriod, start, limit});
		
		return {
			currentResults,
			nextResourceBounds: resourceBoundsIfPossible(start + DEFAULT_RESULT_SET_LIMIT, totalResultsCount),
			lastResourceBounds: resourceBoundsIfPossible(totalResultsCount - DEFAULT_RESULT_SET_LIMIT, totalResultsCount),
		};
	}
}
