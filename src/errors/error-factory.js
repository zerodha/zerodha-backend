
export default class ErrorFactory {
	static convertToApiError(err) {
		return {
			httpStatusCode: 500,
			message: err.message,
			apiErrorCode: 1001
		};
	}
}
