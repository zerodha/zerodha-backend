import ErrorFactory from './error-factory';


const tryToMapToApiErro = err => {
	if (err.toApiError) {
		return err.toApiError();
	}
	return ErrorFactory.convertToApiError(err);
};

export default (err, req, res, next) => {
	console.error(err);

	const apiError = tryToMapToApiErro(err);
	if (apiError) {
		const {httpStatusCode, message, apiErrorCode} = apiError;
		res.status(httpStatusCode)
			.json({message, apiErrorCode});
		return;
	}
	
	res.status(err.status || 500).send();
};
