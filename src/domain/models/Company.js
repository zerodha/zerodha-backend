import {byComparingDescending} from "../../utils";

export default class Company {
	symbol;
	insights;
	
	constructor(symbol, {companyInfo, corporateActions, corporateAnnouncements}) {
		this.symbol = symbol;
		this.insights = {
			companyInfo,
			corporateActions: this._materializeDates(corporateActions),
			corporateAnnouncements
		};
		this._sortActionsByDate();
	}
	
	_materializeDates = corporateActions => ({
		...corporateActions,
		actions: corporateActions.actions.map(
			t => ({...t, exDate: new Date(t.exDate)}))
	});
	
	_sortActionsByDate() {
		this.insights.corporateActions.actions.sort(byComparingDescending("exDate"));
	}
	
	getInsights = () => this.insights;
}
