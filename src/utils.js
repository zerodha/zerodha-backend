import {format, parse, URLSearchParams} from "url";

export const catchUnhandledErrorsAndForward = (target, key, descriptor) => {
	const oldMethod = descriptor.value;
	
	descriptor.value = async function (req, res, next) {
		try {
			const result = oldMethod.call(this, req, res, next);
			if (result instanceof Promise)
				return await result;
			return result;
		} catch (e) {
			return next(e);
		}
	};
	
};


export const resolveJsonPath = (obj, ...keys) => keys.reduce((obj, key) => obj[key], obj);

export const byComparing = (...keys) => (a, b) => {
	const fieldInA = resolveJsonPath(a, keys);
	const fieldInB = resolveJsonPath(b, keys);
	
	if (fieldInA < fieldInB)
		return -1;
	return fieldInA > fieldInB ? 1 : 0;
};

export const byComparingDescending = (...args) => (a, b) => byComparing(...args)(a, b) * -1;


export const replaceQueryParam = (oldRelativeUrl, queryPatch) => {
	const urlObj = parse(oldRelativeUrl);
	const urlSearchParams = new URLSearchParams(urlObj.search);
	Object.entries(queryPatch).forEach(([k, v]) => urlSearchParams.set(k, v));
	urlObj.search = urlSearchParams.toString();
	return format(urlObj);
};

export const constructLinkHeader = (linksMap) => Object.entries(linksMap)
	.filter(([k, v]) => !!v)
	.map(([rel, url]) => `<${url}>; rel="${rel}"`)
	.join(", ");
