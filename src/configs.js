export const NSE_HOST = process.env.NSE_HOST || "https://www1.nseindia.com";

export const _DDOS_PROOF_HEADERS = {
	"user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
	"Accept": "*/*",
	"Accept-Encoding": "gzip, deflate, br",
	"Accept-Language": "en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7",
	"Connection": "keep-alive",
	"X-Requested-With": "XMLHttpRequest",
	"Origin": "https://www1.nseindia.com",
};
