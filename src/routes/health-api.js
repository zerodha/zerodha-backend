import {GET, route} from 'awilix-express';


@route('/health')
export default class HealthAPI {
	/**
	 * @swagger
	 *
	 * /health:
	 *   get:
	 *     description: Get health status of application
	 *     produces:
	 *       - application/json
	 *     responses:
	 *       200:
	 *         description: health
	 */
	@GET()
	getHealth(req, res) {
		res.json({status: 'UP'});
	}
}
