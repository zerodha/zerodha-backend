import {GET, route} from 'awilix-express';
import {catchUnhandledErrorsAndForward, constructLinkHeader, replaceQueryParam} from "../utils";

const LISTING_PERIOD_SYMBOLS = Object.freeze({
	L1M: 'Last 1 Month',
	L6M: 'Last 6 Months',
	L1Y: 'Last 1 Year',
	L2Y: 'Last 2 Years',
	L5Y: 'Last 5 Years',
	MT5Y: 'More than 5 Years'
});

@route('/api/equities')
export default class CompanyAPI {
	_companyService;
	
	
	constructor({companyService}) {
		this._companyService = companyService;
	}
	
	
	@GET()
	@catchUnhandledErrorsAndForward
	async searchListedEquities(req, res) {
		const {
			currentResults,
			nextResourceBounds,
			lastResourceBounds,
		} = await this._companyService.searchEquities({
			...req.query,
			start: req.query.start && parseInt(req.query.start),
			limit: req.query.limit && parseInt(req.query.limit),
			listingPeriod: LISTING_PERIOD_SYMBOLS[req.query["listing-period"]]
		});
		
		const linkHeader = constructLinkHeader({
			next: nextResourceBounds && replaceQueryParam(req.originalUrl, nextResourceBounds),
			last: lastResourceBounds && replaceQueryParam(req.originalUrl, lastResourceBounds),
		});
		res.header("Link", linkHeader);
		res.json(currentResults);
	}
}
