import {GET, route} from 'awilix-express';
import {catchUnhandledErrorsAndForward} from "../utils";

/**
 * @swagger
 *
 * definitions:
 *   CompanyInfo:
 *     type: object
 *     properties:
 *       title:
 *         type: string
 *       info:
 *         type: array
 *         items:
 *           type: object
 *       disclaimer:
 *         type: string
 *
 *   CorporateActions:
 *     type: object
 *     properties:
 *       header:
 *         type: string
 *       actions:
 *         type: array
 *         items:
 *           type: object
 *
 *   CorporateAnnouncements:
 *     type: array
 *     items:
 *       type: object
 *       properties:
 *         url:
 *           type: string
 *           pattern: uri
 *         details:
 *           type: string
 *
 *   CompanyInsights:
 *     type: object
 *     properties:
 *       companyInfo:
 *         $ref: "#/definitions/CompanyInfo"
 *       corporateActions:
 *         $ref: "#/definitions/CorporateActions"
 *       corporateAnnouncements:
 *         $ref: "#/definitions/CorporateAnnouncements"
 *
 */


@route('/api/companies')
export default class CompanyAPI {
	_companySearchRepository;
	_companyService;
	
	
	constructor({companySearchRepository, companyService}) {
		this._companySearchRepository = companySearchRepository;
		this._companyService = companyService;
	}
	
	/**
	 * @swagger
	 *
	 * /api/companies:
	 *   get:
	 *     description: Search company names only
	 *     produces:
	 *       - application/json
	 *     parameters:
	 *     - in: query
	 *       name: query
	 *       description: "Any text either company ticker symbol or name"
	 *       required: false
	 *       schema:
	 *         type: string
	 *         example: "TCS"
	 *     responses:
	 *       200:
	 *         description: Get the symbol and names of companies
	 *         schema:
	 *           type: array
	 *           items:
	 *             type: object
	 *             properties:
	 *               symbol:
	 *                 type: string
	 *               companyName:
	 *                 type: string
	 *           example: {"symbol":"TATAMOTORS","companyName":"Tata Motors Limited"}
	 *
	 */
	@GET()
	@catchUnhandledErrorsAndForward
	async searchCompanies(req, res) {
		const textQuery = req.query.query;
		const results = await this._companySearchRepository.findCompanyMetas(textQuery);
		res.json(results);
	}
	
	/**
	 * @swagger
	 *
	 * /api/companies/{symbol}/insights:
	 *   get:
	 *     description: Get insights of a company
	 *     produces:
	 *       - application/json
	 *     parameters:
	 *     - in: path
	 *       name: symbol
	 *       description: "Id of company i.e. ticker symbol "
	 *       required: true
	 *       schema:
	 *         type: string
	 *         example: "TCS"
	 *     responses:
	 *       200:
	 *         description: Get company info, actions and corporate announcements
	 *         schema:
	 *           $ref: "#/definitions/CompanyInsights"
	 *
	 */
	@GET()
	@route('/:symbol/insights')
	@catchUnhandledErrorsAndForward
	async getCompanyInsights(req, res) {
		res.json(await this._companyService.getCompanyInsightsBySymbol(req.params.symbol));
	}
}
