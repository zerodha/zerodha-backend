const isNotEmpty = (s) => s.trim().length > 1;

export default class CompanyInfoParser {
	
	parseCompanyMetas(restResponseBody) {
		if (!restResponseBody.rows1) {
			return [];
		}
		return restResponseBody.rows1
			.map(t => t["CompanyValues"].split(/\s+(.+)/))
			.map(([symbol, companyName]) => ({symbol, companyName}));
	}
	
	parseEquitySearchResults(restResponseBody) {
		if (!restResponseBody || !restResponseBody.rows) {
			return {totalResultsCount: 0, currentResults: []};
		}
		
		return {
			totalResultsCount: restResponseBody.results,
			currentResults: restResponseBody.rows.map(
				t => ({
					listingDate: t.ListDt,
					companyName: t.cmp,
					faceValue: t.face,
					industry: t.ind,
					isin: t.isin,
					marketLot: t.mlot,
					paidUpValue: t.paid,
					symbol: t.sym,
				})
			)
		};
	}
	
	parseCompanyInfo(unparsedObj) {
		const nonEmptyRows = unparsedObj.map(t => t.row).filter(isNotEmpty);
		return {
			title: nonEmptyRows[0],
			info: this._extractInfoMap(nonEmptyRows),
			disclaimer: nonEmptyRows[nonEmptyRows.length - 1]
		}
	}
	
	parseCorporateActions(unparsedObj) {
		const nonEmptyRows = unparsedObj
			.map(t => t.replace(/[\n\t]/g, '').trim())
			.filter(isNotEmpty);
		if (nonEmptyRows.length === 0) {
			console.warn("Got invalid corporate actions object", unparsedObj);
			return {header: [], actions: []};
		}
		return {
			header: nonEmptyRows[0].split(/\s+/),
			actions: this._extractActions(nonEmptyRows.slice(1)),
		}
	}
	
	
	parseCorporateAnnouncements(unparsedObj, host) {
		return unparsedObj
			.map(({link, cells}) => {
				const nonEmptyCells = cells
					.map(t => t.replace(/[\n\t]/g, '').trim())
					.filter(isNotEmpty);
				return {
					url: host + link,
					details: nonEmptyCells.join(" ")
				};
			});
	}
	
	_extractActions = actionRows => actionRows
		.map(t => t.split(/:([\s\S]*)/))
		.map(([dt, purpose]) => ({exDate: dt.trim(), purpose: purpose.trim()}));
	
	_extractInfoMap = nonEmptyRows => nonEmptyRows
		.slice(1, nonEmptyRows.length - 1)
		.map(t => t.split(/:(.+)/))
		.map(([k, v]) => ({key: k.trim(), value: v.trim()}));
}
