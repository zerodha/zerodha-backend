import AbstractRestClient from "./AbstractRestClient";
import json5 from "json5";

const LIST_DIRECTORY_PAGE_REFERRER = Object.freeze({"Referer": "https://www1.nseindia.com/corporates/listDir/listDirectory.html"});

export default class CompanySearchRepository extends AbstractRestClient {
	_companyInfoParser;
	
	constructor({companyInfoParser}) {
		super();
		this._companyInfoParser = companyInfoParser;
	}
	
	async findCompanyMetas(query) {
		const responseBody = await super.postAsFormBody("/corporates/common/getCompanyList.jsp", {query},
			{}, LIST_DIRECTORY_PAGE_REFERRER);
		return this._companyInfoParser.parseCompanyMetas(responseBody);
	}
	
	async findEquities({symbol, listingPeriod, start, limit}) {
		const responseBody = await super.postAsFormBody("/corporates/listDir/getListDirectEQ.jsp", {
			symbol, listingPeriod, start, limit, segment: "EQUITIES"
		}, {
			transform: body => json5.parse(body)
		}, LIST_DIRECTORY_PAGE_REFERRER);
		return this._companyInfoParser.parseEquitySearchResults(responseBody);
	}
}
