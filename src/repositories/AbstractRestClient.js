import request from "request-promise-native";
import {_DDOS_PROOF_HEADERS, NSE_HOST} from "../configs";


export default class AbstractRestClient {
	postAsFormBody(api, form, options = {}, headers = {}) {
		return request.post({
			url: NSE_HOST + api,
			form, json: true, gzip: true,
			headers: {..._DDOS_PROOF_HEADERS, ...headers},
			...options
		});
	}
}
