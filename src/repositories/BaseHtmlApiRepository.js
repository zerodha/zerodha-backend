import osmosis from "osmosis";
import {_DDOS_PROOF_HEADERS, NSE_HOST} from "../configs";


export default class BaseHtmlApiRepository {
	static fetchAndParseHtml(
		{
			method = 'get', api,
			url = NSE_HOST + api,
			queryParams = {}, headers = {}, operations
		}) {
		return new Promise(resolve => this._fetchHtml(url, queryParams, headers)
			.set(operations)
			.data(resolve)
		);
	}
	
	static _fetchHtml = (url, queryParams, headers) => osmosis
		.get(url, queryParams)
		.headers({..._DDOS_PROOF_HEADERS, ...headers});
	
	static ops({selector, schema}) {
		return osmosis.find(selector).set(schema);
	}
	
	static handler({selector, extractor}) {
		return osmosis.find(selector).then((ctx, data, next, done) => {
				Object.assign(data, extractor(ctx));
				done();
			}
		);
	}
	
	static arrayOps = (...args) => [this.ops(...args)];
	
	static arrayHandler = (...args) => [this.handler(...args)];
}
