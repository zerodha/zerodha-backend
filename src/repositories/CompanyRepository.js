import BaseHtmlApiRepository from "./BaseHtmlApiRepository";
import {NSE_HOST} from "../configs";

const extractLinkFromJsWindowOpenFn = onClick => /window\.open\('(?<link>[^']*)/.exec(onClick).groups.link;

const CM_TRACKER_PAGE_REFERRER = Object.freeze({headers: {"Referer": "https://www1.nseindia.com/companytracker/cmtracker.jsp?symbol=INFY&cName=cmtracker_nsedef.css"}});

export default class CompanyRepository extends BaseHtmlApiRepository {
	_companyInfoParser;
	
	constructor({companyInfoParser}) {
		super();
		this._companyInfoParser = companyInfoParser;
	}
	
	async getCompanyInfo({symbol}) {
		const parsedHtml = await CompanyRepository.fetchAndParseHtml({
			api: "/marketinfo/companyTracker/compInfo.jsp",
			queryParams: {symbol, series: "EQ"},
			...CM_TRACKER_PAGE_REFERRER,
			operations: CompanyRepository.arrayOps({
				selector: "tr",
				schema: {row: "td"}
			}),
		});
		return this._companyInfoParser.parseCompanyInfo(parsedHtml);
	}
	
	async getCorporateActions({symbol}) {
		const parsedHtml = await CompanyRepository.fetchAndParseHtml({
			api: "/marketinfo/companyTracker/corpAction.jsp",
			queryParams: {symbol},
			...CM_TRACKER_PAGE_REFERRER,
			operations: ["tr"],
		});
		return this._companyInfoParser.parseCorporateActions(parsedHtml);
	}
	
	async getCorporateAnnouncements({symbol}) {
		const parsedHtml = await CompanyRepository.fetchAndParseHtml({
			api: "/marketinfo/companyTracker/corpAnnounce.jsp",
			queryParams: {symbol},
			...CM_TRACKER_PAGE_REFERRER,
			operations: CompanyRepository.arrayHandler({
				selector: "tr",
				extractor: (ctx) => ({
					link: extractLinkFromJsWindowOpenFn(
						ctx.querySelector("a").onclick),
					cells: ctx.querySelectorAll("td")
						.map(t => t.innerText),
				})
			}),
		});
		return this._companyInfoParser.parseCorporateAnnouncements(parsedHtml, NSE_HOST);
	}
}
