#!/usr/bin/env node

/**
 * Module dependencies.
 */

import {initApp} from '../app';
import debugLib from 'debug';
import http from 'http';

const debug = debugLib('smallcase-backend:server');


/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      /* eslint-disable no-console */
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
const onListening = (server) => () => {
  var addr = server.address();
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  console.log(`Listening on ${bind}`);
  debug('Listening on ' + bind);
};

const tillServerIsListening = (server) => new Promise(resolve => {
  const serverIsAlreadyListening = !!server.address();
  if (serverIsAlreadyListening) {
    resolve();
  }
  server.on('listening', resolve);
});


export const serverPromise = initApp().then(async ({app, container}) => {
  /**
   * Get port from environment and store in Express.
   */
  
  const port = normalizePort(process.env.PORT || '3000');
  
  app.set('port', port);
  /**
   * Create HTTP server.
   */
  
  const server = http.createServer(app);
  
  server.on('error', onError);
  server.on('listening', onListening(server));
  
  /**
   * Listen on provided port, on all network interfaces.
   */
  server.listen(port);
  
  await tillServerIsListening(server);
  return {server, app, container};
})
    .catch(console.error);


