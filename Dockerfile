FROM node:10.15.3-jessie-slim

RUN mkdir -p /zerodha-backend-task/
WORKDIR /zerodha-backend-task


ADD .npmrc .
ADD package.json .
#Install modules for major version and cache it -- saves time if package-lock is changed
RUN npm install

ADD package-lock.json .
#Fine tune installing modules with minor version and cache it
RUN npm install


#Add our sources and build
ADD . .
RUN npm run build

#Run tests only if required
ARG RUN_TESTS
RUN [ -z $RUN_TESTS ] || npm run test

EXPOSE 3000

CMD NODE_ENV=production node ./dist/bin/www
